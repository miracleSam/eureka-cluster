package com.eureka.provider;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.eureka.provider.lock.ZookeeperLock;
import com.eureka.provider.mapper.*;
import com.eureka.provider.pojo.Order;
import com.eureka.provider.pojo.SeckillGoods;
import com.eureka.provider.pojo.SeckillOrder;
import com.eureka.provider.pojo.User;
import com.eureka.provider.service.ISeckillGoodsService;
import com.eureka.provider.utils.SerializeUtil;
import com.eureka.provider.utils.UserGeneratorUtil;
import com.eureka.provider.utils.fastdfsUtil;
import com.eureka.provider.vo.GoodsVo;
import org.apache.curator.framework.recipes.locks.InterProcessMutex;
import org.csource.common.MyException;
import org.csource.common.NameValuePair;
import org.csource.fastdfs.*;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.kafka.core.KafkaTemplate;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;
import java.util.concurrent.TimeUnit;

@SpringBootTest
class ProviderApplicationTests {

    /*
    fastdfs测试
     */
    @Test
    void contextLoads() throws IOException, MyException {
        ClientGlobal.initByProperties("fastdfs.properties");
        TrackerClient trackerClient = new TrackerClient();
        TrackerServer trackerServer=trackerClient.getConnection();
        StorageServer storageServer=null;
        StorageClient1 storageClient1 = new StorageClient1(trackerServer, storageServer);
        //元数据
        NameValuePair nameValuePair[] = null;
        String fileId = storageClient1.upload_file1("C:\\Users\\67409\\Pictures\\234.jpeg", "jpeg", nameValuePair);
        System.out.println(fileId);
    }
    @Autowired
    fastdfsUtil fastdfsUtil;
    @Test
    void Test() throws IOException, MyException {
        String file="C:\\Users\\67409\\Pictures\\234.jpeg";
        fastdfsUtil.upload(file,"jpeg");
    }
    /*
    商品接口测试
     */
    @Autowired
    GoodsMapper goodsMapper;
    @Autowired
    SeckillOrderMapper seckillOrderMapper;
    @Autowired
    OrderMapper orderMapper;
    @Test
    void goodsTest(){
//        Order order=new Order();
//        order.setUserId((long) 1);
//        order.setGoodsId((long) 1);
//        orderMapper.insert(order);
        //生成秒杀订单
        SeckillOrder seckillOrder = new SeckillOrder();
        seckillOrder.setOrderId((long) 1);
        seckillOrder.setUserId((long) 1);
        seckillOrder.setGoodsId((long) 1);
        System.out.println(seckillOrder.toString());
        seckillOrderMapper.insert(seckillOrder);
    }
    /*
    生成随机用户
     */
    @Autowired
    UserGeneratorUtil userGeneratorUtil;
    @Autowired
    RedisTemplate redisTemplate;
    @Test
    void generateUser() throws IOException {
        int count=5000;
        userGeneratorUtil.generateUsers(count);
    }
    @Test
    void UserLogin() throws IOException {
        userGeneratorUtil.doLogin("C:\\Users\\67409\\Desktop\\usersTable.txt",5000);
    }
    /**
     * 用户测试
     */
    @Autowired
    UserMapper userMapper;
    @Autowired
    ISeckillGoodsService seckillGoodsService;
    @Test
    void user(){
        int count=0;
        for (int i = 0; i < 100; i++) {
            SeckillGoods seckillGoods = seckillGoodsService.getOne(new QueryWrapper<SeckillGoods>()
                    .eq("goods_id", 3));
            if(seckillGoods.getSeckillStock()<1) continue;
            boolean update = seckillGoodsService.update(new UpdateWrapper<SeckillGoods>()
                    .set("seckill_stock", seckillGoods.getSeckillStock()-1)
                    .eq("id", seckillGoods.getId())
                    .gt("seckill_stock", 0));
            if(update) count++;
            System.out.println(""+i+update);
        }
        System.out.println(count);
    }
    /**
     * kafka测试
     */
    @Autowired
    KafkaTemplate kafkaTemplate;
    @Test
    public void kafka(){
        Order order=new Order();
        order.setId(1L);
//        String x=JSON.toJSONString(order);
//        System.out.println(x);
//        Order o = JSON.parseObject(x,Order.class);
//        System.out.println(o.toString());
        kafkaTemplate.send("order", JSON.toJSONString(order));
//        byte[] serialize = SerializeUtil.serialize(order);
//        System.out.println(serialize.toString());
//        Order deserialize = (Order) SerializeUtil.deserialize(serialize);
//        System.out.println(deserialize.toString());


    }

    /**
     * 乐观锁测试
     */
    @Autowired
    SeckillGoodsMapper seckillGoodsMapper;
    @Test
    public void optimisticLock(){
        SeckillGoods seckillGoods= seckillGoodsMapper.selectById(3);
        seckillGoods.setSeckillStock(seckillGoods.getSeckillStock()-1);
        seckillGoodsMapper.updateById(seckillGoods);
        while (true){
            SeckillGoods goods2=seckillGoodsMapper.selectById(3);
            seckillGoods.setSeckillStock(seckillGoods.getSeckillStock()-1);
            seckillGoodsMapper.updateById(goods2);
        }
    }
    /**
     * redis测试
     */
    @Value("${zookeeper.host}")
    public String zookeeperHost;
    @Test
    public void redis(){
//        int stock=(int)redisTemplate.opsForValue().get("stock:"+3);
//        System.out.println(stock);
        System.out.println(zookeeperHost);
    }

    /**
     * zookeeper lock测试
     */
    static class TestThread implements Runnable {
        private Integer threadFlag;
        private InterProcessMutex lock;

        public TestThread(Integer threadFlag, InterProcessMutex lock) {
            this.threadFlag = threadFlag;
            this.lock = lock;
        }

        @Override
        public void run() {
            try {
                ZookeeperLock.acquire(1,TimeUnit.SECONDS);
                System.out.println("第"+threadFlag+"线程获取到了锁");
                //等到1秒后释放锁
                Thread.sleep(1000);
            } catch (Exception e) {
                e.printStackTrace();
            }finally {
                try {
                   ZookeeperLock.release();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }
    @Test
    public void zookeeper(){
        for(int i=0;i<100;i++){
            new Thread(new TestThread(i,ZookeeperLock.getMutex())).start();
        }
    }

}
