package com.eureka.provider.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.eureka.provider.pojo.SeckillGoods;
import com.eureka.provider.pojo.User;
import com.eureka.provider.vo.RespBean;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author sam
 * @since 2021-04-15
 */
public interface ISeckillGoodsService extends IService<SeckillGoods> {


    RespBean doSecKill1(User user, Long goodsId);

    RespBean doSecKill2(User user, Long goodsId);

    RespBean doSecKill3(User user, Long goodsId);
}
