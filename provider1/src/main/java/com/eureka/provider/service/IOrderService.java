package com.eureka.provider.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.eureka.provider.pojo.Order;
import com.eureka.provider.pojo.User;
import com.eureka.provider.vo.GoodsVo;

import java.io.IOException;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author sam
 * @since 2021-04-15
 */
public interface IOrderService extends IService<Order> {

    Order secKill(User user, GoodsVo goods);
}
