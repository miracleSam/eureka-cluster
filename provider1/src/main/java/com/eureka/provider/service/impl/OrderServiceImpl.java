package com.eureka.provider.service.impl;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.eureka.provider.mapper.OrderMapper;
import com.eureka.provider.mapper.SeckillOrderMapper;
import com.eureka.provider.pojo.Order;
import com.eureka.provider.pojo.SeckillGoods;
import com.eureka.provider.pojo.SeckillOrder;
import com.eureka.provider.pojo.User;
import com.eureka.provider.service.IOrderService;
import com.eureka.provider.service.ISeckillGoodsService;
import com.eureka.provider.vo.GoodsVo;
import com.eureka.provider.vo.RespBeanEnum;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.Date;
import java.util.concurrent.locks.ReentrantLock;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author sam
 * @since 2021-04-15
 */
@Service
@Slf4j
public class OrderServiceImpl extends ServiceImpl<OrderMapper, Order> implements IOrderService {

    @Autowired
    OrderMapper orderMapper;
    @Autowired
    ISeckillGoodsService seckillGoodsService;
    @Autowired
    SeckillOrderMapper seckillOrderMapper;
    @Autowired
    KafkaTemplate kafkaTemplate;

    @Override
    @Transactional
    public Order secKill(User user, GoodsVo goods)  {
        //创建订单
        Order order=new Order();
        order.setUserId(user.getId());
        order.setGoodsId(goods.getId());
        order.setGoodsCount(1);
        order.setGoodsPrice(goods.getGoodsPrice());
        order.setCreatDate(new Date());
        order.setStatus(0);
        //orderMapper.insert(order);
        kafkaTemplate.send("order", JSON.toJSONString(order));
        //生成秒杀订单
        SeckillOrder seckillOrder = new SeckillOrder();
        seckillOrder.setOrderId(order.getId());
        seckillOrder.setUserId(user.getId());
        seckillOrder.setGoodsId(goods.getId());
        //seckillOrderMapper.insert(seckillOrder);
        kafkaTemplate.send("seckillOrder",JSON.toJSONString(seckillOrder));
        log.info(user.getId()+":"+goods.getId()+"==>"+ RespBeanEnum.SUCCESS.getMsg());
        return order;
    }
}
