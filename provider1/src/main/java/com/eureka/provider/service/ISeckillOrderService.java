package com.eureka.provider.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.eureka.provider.pojo.SeckillOrder;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author sam
 * @since 2021-04-15
 */
public interface ISeckillOrderService extends IService<SeckillOrder> {

}
