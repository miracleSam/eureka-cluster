package com.eureka.provider.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.eureka.provider.mapper.UserMapper;
import com.eureka.provider.pojo.User;
import com.eureka.provider.service.IUserService;
import com.eureka.provider.utils.CookieUtil;
import com.eureka.provider.utils.MD5Util;
import com.eureka.provider.utils.UUIDUtil;
import com.eureka.provider.vo.LoginVo;
import com.eureka.provider.vo.RespBean;
import com.eureka.provider.vo.RespBeanEnum;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Date;
import java.util.Enumeration;

/**
 * <p>
 *  用户服务实现类
 * </p>
 *
 * @author sam
 * @since 2021-04-14
 */
@Service
@Slf4j
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements IUserService {

    @Autowired
    UserMapper userMapper;
    @Autowired
    RedisTemplate redisTemplate;

    @Override
    public RespBean login(LoginVo loginVo, HttpServletRequest request, HttpServletResponse response) {
        String username=loginVo.getUsername();
        String password=loginVo.getPassword();
        if(username==null || password==null) {
            return RespBean.Error(RespBeanEnum.LOGIN_NULL_ERROR);
        }
        User user=userMapper.selectById(username);
        if(user==null || !user.getPassword().equals(MD5Util.inputPassToDbPass(password,MD5Util.salt))){
            System.out.println("输入密码："+MD5Util.inputPassToDbPass(password,MD5Util.salt));
            return RespBean.Error(RespBeanEnum.LOGIN_ERROR);
        }
        //log.info("",loginVo);
        String uuid = UUIDUtil.uuid();
        //request.getSession().setAttribute(uuid,user);
        //分布式session
        redisTemplate.opsForValue().set("user:"+uuid,user);
        //存储到用户cookie中
        CookieUtil.setCookie(request,response,"userTicket",uuid);
        //登录次数加1,更新最后登录日期
        user.setLoginCount(user.getLoginCount()+1);
        user.setLastLoginDate(new Date());
        userMapper.updateById(user);
        return RespBean.Success(uuid);
    }

    @Override
    public User getUserByCookie(String userTicket,HttpServletRequest request,HttpServletResponse response) {
        if(userTicket.isEmpty()){
            return null;
        }
        User user=(User) redisTemplate.opsForValue().get("user:"+userTicket);
        if (user!=null){
            CookieUtil.setCookie(request,response,"userTicket",userTicket);
        }
        return user;
    }

    @Override
    public User getUserByCookie(String userTicket) {
        User user=(User) redisTemplate.opsForValue().get("user:"+userTicket);
        return user;
    }

    @Override
    public String login(User user) {
        String username=user.getNikename();
        String password=user.getPassword();
        if(username==null || password==null) {
            return null;
        }
        String uuid = UUIDUtil.uuid();
        //分布式session
        redisTemplate.opsForValue().set("user:"+uuid,user);
        return uuid;

    }
}
