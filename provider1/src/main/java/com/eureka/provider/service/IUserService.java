package com.eureka.provider.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.eureka.provider.pojo.User;
import com.eureka.provider.vo.LoginVo;
import com.eureka.provider.vo.RespBean;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author sam
 * @since 2021-04-14
 */
public interface IUserService extends IService<User> {

    RespBean login(LoginVo loginVo, HttpServletRequest request, HttpServletResponse response);

    //根据cookie获取用户

    User getUserByCookie(String userTicket, HttpServletRequest request, HttpServletResponse response);

    User getUserByCookie(String userTicket);

    String login(User user);
}
