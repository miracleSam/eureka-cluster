package com.eureka.provider.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.eureka.provider.pojo.Goods;
import com.eureka.provider.pojo.Order;
import com.eureka.provider.pojo.User;
import com.eureka.provider.vo.GoodsVo;
import com.eureka.provider.vo.RespBean;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author sam
 * @since 2021-04-15
 */
public interface IGoodsService extends IService<Goods> {
    /**
     * 获取商品列表
     * @return
     */

    List<GoodsVo> findGoodsVo();

    GoodsVo findGoodsVoById(Long id);

    GoodsVo findGoodsVoByIdRedis(Long id);

    RespBean doSecKill3(User user, Long goodsId);

    @Transactional
    RespBean doSecKill4(User user, Long goodsId);

    RespBean doSecKill2(User user, Long goodsId);

    RespBean doSecKillSync(User user, Long goodsId) throws Exception;

    RespBean doSecKill5(User user, Long goodsId);

    RespBean doSecKill6(User user, Long goodsId);
}
