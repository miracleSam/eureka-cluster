package com.eureka.provider.queue.jvm.kafka;/*

 *@Description
 *@Author:Sam
 *@creat-time:2021/4/19 15:31
 */
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import org.apache.kafka.common.serialization.Deserializer;

import java.util.Map;

public class JSONDeserializer implements Deserializer<Object> {
    @Override
    public void configure(Map<String, ?> configs, boolean isKey) {

    }

    @Override
    public JSONObject deserialize(String s, byte[] bytes) {
        return JSON.parseObject(bytes,Object.class);
    }

    @Override
    public void close() {

    }
}
