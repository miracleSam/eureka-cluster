package com.eureka.provider.queue.jvm.kafka;/*
 *@Description
 *@Author:Sam
 *@creat-time:2021/4/19 15:03
 */

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import org.apache.kafka.common.serialization.Serializer;

import java.util.Map;

public class JSONSerializer implements Serializer<Object> {

    @Override
    public void configure(Map<String, ?> configs, boolean isKey) {

    }

    @Override
    public byte[] serialize(String s, Object obj) {
        return JSON.toJSONBytes(obj);
    }

    @Override
    public void close() {

    }
}

