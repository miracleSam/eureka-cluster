package com.eureka.provider.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author sam
 * @since 2021-04-15
 */
@RestController
@RequestMapping("/seckill_order")
public class SeckillOrderController {

}
