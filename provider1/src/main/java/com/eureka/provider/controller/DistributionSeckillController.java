package com.eureka.provider.controller;/*
 *@Description
 *@Author:Sam
 *@creat-time:2021/4/20 14:42
 */

import com.eureka.provider.pojo.User;
import com.eureka.provider.service.ISeckillGoodsService;
import com.eureka.provider.service.impl.SeckillGoodsServiceImpl;
import com.eureka.provider.vo.RespBean;
import com.eureka.provider.vo.RespBeanEnum;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/distri_seckill")
@Slf4j
public class DistributionSeckillController {

    @Autowired
    ISeckillGoodsService goodsService;

    /**
     * Resdison锁
     * QPS 84
     * @param user
     * @param goodsId
     * @return
     */
    @RequestMapping("/doSecKill1")
    public RespBean doSecKill1(User user, Long goodsId) {
        if(user==null){
            return RespBean.Error(RespBeanEnum.NOT_LOGIN);
        }
        return goodsService.doSecKill1(user,goodsId);
    }

    /**
     * 秒杀2:redis原子递减
     * QPS 1340
     * @param user
     * @param goodsId
     * @return
     */

    @RequestMapping("/doSecKill2")
    public RespBean doSecKill2(User user, Long goodsId) {
        if(user==null){
            return RespBean.Error(RespBeanEnum.NOT_LOGIN);
        }
        return goodsService.doSecKill2(user,goodsId);
    }

    /**
     * 秒杀3：zookeeper分布式锁
     * @param user
     * @param goodsId
     * @return
     */

    @RequestMapping("/doSecKill3")
    public RespBean doSecKill3(User user, Long goodsId) {
        if(user==null){
            return RespBean.Error(RespBeanEnum.NOT_LOGIN);
        }
        return goodsService.doSecKill3(user,goodsId);
    }
}
