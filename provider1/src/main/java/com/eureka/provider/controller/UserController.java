package com.eureka.provider.controller;


import com.eureka.provider.pojo.User;
import com.eureka.provider.service.IUserService;
import com.eureka.provider.vo.RespBean;
import com.eureka.provider.vo.RespBeanEnum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author sam
 * @since 2021-04-14
 */
@RestController
@RequestMapping("/user")
public class UserController {
    @Autowired
    IUserService userService;

    /**
     * 用户信息测试接口
     * @param user
     * @return
     */

    @RequestMapping("/info")
    public RespBean info(User user){
        //User user=userService.getUserByCookie(userTicket);
        if(user==null){
            return RespBean.Error(RespBeanEnum.NOT_LOGIN);
        }
        System.out.println(user.toString());
        return RespBean.Success(user);
    }
    /*
    id password userTicket
    2 9e703efae03fc2a6ddaf1816e550d3f6 64169d0c6156493a93449c0998cbf041
    123 9e703efae03fc2a6ddaf1816e550d3f6 52f0acf761c3403d86fae143ab1c4b8d
     */

}
