package com.eureka.provider.controller;/*
 *@Description
 *@Author:Sam
 *@creat-time:2021/4/14 14:34
 */

import com.eureka.provider.service.IUserService;
import com.eureka.provider.service.impl.UserServiceImpl;
import com.eureka.provider.vo.LoginVo;
import com.eureka.provider.vo.RespBean;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

@Controller
@Slf4j
public class LoginController {
    @Autowired
    IUserService userService;

    /**
     * 跳转登录页面
     * @return
     */
    @RequestMapping("/login")
    public String toLogin(){
        return "login";
    }

    /**
     * 登录实现
     * @param loginVo
     * @return
     */
    @RequestMapping("/doLogin")
    @ResponseBody
    public RespBean doLogin(@Valid LoginVo loginVo, HttpServletRequest request, HttpServletResponse response){

        return userService.login(loginVo,request,response);
    }
}
