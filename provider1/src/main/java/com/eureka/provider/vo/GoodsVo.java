package com.eureka.provider.vo;/*
 *@Description
 *@Author:Sam
 *@creat-time:2021/4/15 15:39
 */

import com.eureka.provider.pojo.Goods;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

@Data
public class GoodsVo extends Goods {

    /**
     * 秒杀库存
     */
    private Integer seckillStock;

    /**
     * 秒杀价
     */
    private BigDecimal seckillPrice;

    /**
     * 秒杀开始时间
     */
    private Date startDate;

    /**
     * 秒杀结束时间
     */
    private Date endDate;
    /**
     * 秒杀状态
     * 1 未开始
     * 2 正在进行
     * 3 已结束
     */
    private int state=1;
    /**
     * 距离开始的时间
     */
    private int remainTime=0;
}
