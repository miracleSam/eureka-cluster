package com.eureka.provider.vo;/*
 *@Description
 *@Author:Sam
 *@creat-time:2021/4/14 15:03
 */

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class RespBean {
    private long code;
    private String msg;
    private Object obj;

    /**
     * 成功返回
     * @return
     */
    public static RespBean Success(){
        return new RespBean(RespBeanEnum.SUCCESS.getCode(),RespBeanEnum.SUCCESS.getMsg(), null);
    }

    public static RespBean Success(Object obj){
        return new RespBean(RespBeanEnum.SUCCESS.getCode(),RespBeanEnum.SUCCESS.getMsg(), obj);
    }

    public static RespBean Error(){
        return new RespBean(RespBeanEnum.ERROR.getCode(), RespBeanEnum.ERROR.getMsg(),null);
    }
    public static RespBean Error(RespBeanEnum respBeanEnum){
        return new RespBean(respBeanEnum.getCode(), respBeanEnum.getMsg(),null);
    }
}
