package com.eureka.provider.vo;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;


@ToString
@AllArgsConstructor
@Getter
public enum RespBeanEnum {
    SUCCESS(200,"SUCCESS"),
    ERROR(500,"服务端异常"),
    //登录
    LOGIN_NULL_ERROR(501,"用户或密码为空"),
    LOGIN_ERROR(502,"用户或密码错误"),
    NOT_LOGIN(503,"用户未登录"),
    //秒杀
    EMPTY_STOCK(504,"库存不足"),
    MUTIL_ORDER(505,"限购一件");

    private final Integer code;
    private final String msg;
}
