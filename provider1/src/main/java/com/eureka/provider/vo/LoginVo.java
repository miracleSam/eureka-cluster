package com.eureka.provider.vo;/*
 *@Description
 *@Author:Sam
 *@creat-time:2021/4/14 15:19
 */

import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;

@Data
public class LoginVo {
    @NotNull
    String username;
    @NotNull
    //@Length(min = 32)
    String password;
}
