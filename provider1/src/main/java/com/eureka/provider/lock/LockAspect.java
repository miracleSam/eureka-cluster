package com.eureka.provider.lock;/*
 *@Description
 *@Author:Sam
 *@creat-time:2021/4/19 17:45
 */

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

@Component
@Aspect
@Scope
public class LockAspect {
    Lock lock=new ReentrantLock(true);

    @Pointcut("@annotation(com.eureka.provider.lock.ServiceLock)")
    public void LockAspect(){

    }
    @Around("LockAspect() && @annotation(name)")
    @Transactional
    public Object around(ProceedingJoinPoint joinPoint,ServiceLock name){
        lock.lock();
        Object obj=null;
        //System.out.println("调用方法为:"+name.value());
//        System.out.println(joinPoint.getSignature()+":"+joinPoint.getArgs()+":"+joinPoint.getKind()+":"+
//                joinPoint.getSourceLocation()+":"+joinPoint.getTarget());
        try {
            obj=joinPoint.proceed();
        }catch (Throwable e){
            e.printStackTrace();
        }finally {
            lock.unlock();
        }
        return obj;
    }
}
