package com.eureka.provider.proxy;/*
 *@Description
 *@Author:Sam
 *@creat-time:2021/4/12 14:14
 */

public class CalculatorImp implements Calculator {

    @Override
    public int add(int a, int b) {
        return a+b;
    }
}
