package com.eureka.provider.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.eureka.provider.pojo.Order;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author sam
 * @since 2021-04-15
 */
@Mapper
public interface OrderMapper extends BaseMapper<Order> {

}
