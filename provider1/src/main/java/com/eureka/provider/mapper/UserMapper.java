package com.eureka.provider.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.eureka.provider.pojo.User;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author sam
 * @since 2021-04-14
 */

@Mapper
public interface UserMapper extends BaseMapper<User> {

}
