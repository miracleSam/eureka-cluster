package com.eureka.provider.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.eureka.provider.pojo.SeckillGoods;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author sam
 * @since 2021-04-15
 */
@Mapper
public interface SeckillGoodsMapper extends BaseMapper<SeckillGoods> {


}
