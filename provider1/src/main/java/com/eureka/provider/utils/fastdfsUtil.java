package com.eureka.provider.utils;/*
 *@Description
 *@Author:Sam
 *@creat-time:2021/4/13 16:55
 */

import org.csource.common.MyException;
import org.csource.common.NameValuePair;
import org.csource.fastdfs.*;
import org.springframework.stereotype.Component;

import java.io.IOException;

@Component
public class fastdfsUtil {

    public String upload(String file, String type) throws IOException, MyException {
        ClientGlobal.initByProperties("fastdfs.properties");
        TrackerClient trackerClient = new TrackerClient();
        TrackerServer trackerServer=trackerClient.getConnection();
        StorageServer storageServer=null;
        StorageClient1 storageClient1 = new StorageClient1(trackerServer, storageServer);
        //元数据
        NameValuePair nameValuePair[] = null;
        String fileId = storageClient1.upload_file1(file, type, nameValuePair);
//        byte[] bytes = storageClient1.download_file1(fileId);
//        System.out.println(bytes.length);
        System.out.println(fileId);
        return fileId;
    }
}
