package com.eureka.provider.utils;/*
 *@Description
 *@Author:Sam
 *@creat-time:2021/4/14 16:57
 */

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;

public final class CookieUtil {

    public static String getCookieValue(HttpServletRequest request,String cookieName){

        return getCookieValue(request,cookieName,false);
    }

    private static String getCookieValue(HttpServletRequest request, String cookieName, boolean isDecoder) {
        Cookie[] cookies=request.getCookies();
        if(cookies==null || cookieName==null) {
            return null;
        }
        String retValue=null;
        try {
            for(int i=0;i<cookies.length;i++){
                if(cookies[i].getName().equals(cookieName)){
                    if(isDecoder){
                        retValue= URLDecoder.decode(cookies[i].getValue(),"UTF-8");
                    }else{
                        retValue=cookies[i].getValue();
                    }
                }
                break;

            }
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return retValue;
    }

    public static void setCookie(HttpServletRequest request, HttpServletResponse response, String cookieName, String cookieValue){
        setCookie(request,response,cookieName,cookieValue,-1);
    }

    private static void setCookie(HttpServletRequest request, HttpServletResponse response, String cookieName, String cookieValue, int cookieMaxage) {
        setCookie(request,response,cookieName,cookieValue,cookieMaxage,false);
    }

    private static void setCookie(HttpServletRequest request, HttpServletResponse response, String cookieName, String cookieValue, int cookieMaxage, boolean isEncode) {
        doSetCookie(request,response,cookieName,cookieValue,cookieMaxage,isEncode);
    }

    private static void doSetCookie(HttpServletRequest request, HttpServletResponse response, String cookieName, String cookieValue, int cookieMaxage, boolean isEncode) {
        try {
            if(cookieValue==null) {
                cookieValue="";
            }else if(isEncode){
                cookieValue= URLEncoder.encode(cookieValue,"UTF-8");
            }
            Cookie cookie=new Cookie(cookieName,cookieValue);
            if(cookieMaxage>0){
                cookie.setMaxAge(cookieMaxage);
            }
            if(request!=null){
                String domainName=getDomainName(request);
                //System.out.println(domainName);
                if(!"localhost".equals(domainName)){
                    cookie.setDomain(domainName);
                }
            }
            cookie.setPath("/");
            response.addCookie(cookie);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

    }

    private static String getDomainName(HttpServletRequest request) {
        String domainName=null;
        String serverName=request.getRequestURL().toString();
        if(serverName==null || serverName.equals("")){
            domainName="";
        }
        else{
            serverName=serverName.toLowerCase();
            if(serverName.startsWith("http://")) {
                serverName=serverName.substring(7);
            }
            int end=serverName.length();
            if(serverName.contains("/")){
                end=serverName.indexOf("/");
            }
            serverName=serverName.substring(0,end);
            final String[] domains=serverName.split("\\.");
            int len=domains.length;
            if(len>3){
                domainName=domains[len-3]+"."+domains[len-2]+"."+domains[len-1];
            }else if(len<=3 && len>1){
                domainName=domains[len-2]+"."+domains[len-1];
            }else{
                domainName=serverName;
            }
        }
        if(domainName!=null && domainName.indexOf(":")>0){
            String[] arr=domainName.split("\\:");
            domainName=arr[0];
        }
        return domainName;

    }
}
