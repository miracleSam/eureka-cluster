package com.eureka.provider.utils;/*
 *@Description
 *@Author:Sam
 *@creat-time:2021/4/14 11:19
 */

import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.stereotype.Component;

@Component
public class MD5Util {
    public static String md5(String src){
        return DigestUtils.md5Hex(src);
    }
    public static final String salt="1a2b3c4d";
    public static String inputPassToFromPass(String inputpass){
        String str=salt.charAt(0)+salt.charAt(2)+inputpass+salt.charAt(5)+salt.charAt(4);
        return md5(str);
    }
    public static String fromPasstoDbPass(String fromPass,String salt){
        String str=salt.charAt(1)+salt.charAt(3)+fromPass+salt.charAt(4)+salt.charAt(5);
        return md5(str);
    }
    public static String inputPassToDbPass(String inputpass,String salt){
        String fromPass = inputPassToFromPass(inputpass);
        String dbPass = fromPasstoDbPass(fromPass,salt);
        return dbPass;
    }

    public static void main(String[] args) {
        //ce21b747de5af71ab5c2e20ff0a60eea
        System.out.println(inputPassToFromPass("123456"));
        //9e703efae03fc2a6ddaf1816e550d3f6
        System.out.println(fromPasstoDbPass("ce21b747de5af71ab5c2e20ff0a60eea",salt));
        //9e703efae03fc2a6ddaf1816e550d3f6
        System.out.println(inputPassToDbPass("123456",salt));
    }
}
