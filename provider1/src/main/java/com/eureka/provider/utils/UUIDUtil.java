package com.eureka.provider.utils;/*
 *@Description
 *@Author:Sam
 *@creat-time:2021/4/14 16:57
 */

import java.util.UUID;

public class UUIDUtil {

    public static String uuid(){
        return UUID.randomUUID().toString().replace("-","");
    }

    public static void main(String[] args) {
        System.out.println(UUID.randomUUID().toString());
    }
}
