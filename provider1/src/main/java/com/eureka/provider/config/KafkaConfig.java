package com.eureka.provider.config;/*
 *@Description
 *@Author:Sam
 *@creat-time:2021/4/17 16:59
 */

import org.apache.kafka.clients.admin.NewTopic;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class KafkaConfig {

    @Bean
    public NewTopic creatTopic(){
        return new NewTopic("test",8, (short) 1);
    }
}
